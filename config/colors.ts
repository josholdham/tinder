export const colors = {
  accent: "#00acc1",
  accentSecondary: "#6472bb",
  red: "#e91e63",
};
