import React, { useContext, useState } from "react";
import { StyleSheet, View } from "react-native";
import CardWrapper from "./CardWrapper";
import EmptyCard from "./EmptyCard";
import { HistoryContext } from "./HistoryContext";

type DeckParams = {
  data: any[];
};
function Deck({ data }: DeckParams) {
  const [stack, setStack] = useState<any[]>(data);
  const { setHistory } = useContext(HistoryContext);

  return (
    <View style={styles.container}>
      <EmptyCard
        onReset={() => {
          setStack(data.map((p) => ({ ...p })));
          setHistory([]);
        }}
      />
      {stack.map((profile, i) => {
        return (
          <CardWrapper
            zIndex={stack.length - i}
            key={profile.id}
            profile={profile}
          />
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Deck;
