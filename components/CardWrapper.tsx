import React, { useContext, useEffect } from "react";
import { StyleSheet, Dimensions } from "react-native";
import { PanGestureHandler } from "react-native-gesture-handler";
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from "react-native-reanimated";

import Card from "./Card";
import { HistoryContext } from "./HistoryContext";

import { Profile } from "../types";

type CardWrapperParams = {
  profile: Profile;
  zIndex: number;
};
function CardWrapper({ profile, zIndex = 1 }: CardWrapperParams) {
  const { setHistory, history } = useContext(HistoryContext);

  useEffect(() => {
    reset(false);
  }, [profile]);

  /***************************
   * Constants
   ***************************/
  const { width, height } = Dimensions.get("window");
  const ROTATION_X_RANGE_SIZE = 0.75 * width;
  const ROTATION_DEGREE_RANGE_SIZE = 20;
  const ACTION_THRESHOLD = 0.2 * width;

  /***************************
   * Reanimated Shared Values
   ***************************/
  const translateX = useSharedValue(0);
  const rotation = useSharedValue(0);
  const likeOpacity = useSharedValue(0);
  const nopeOpacity = useSharedValue(0);

  /*******************************************
   * Resetting and Completing Animations
   *******************************************/

  /**
   * This resets the card's animation position.
   * Called in event of a swipe being 'cancelled' (released early)
   *  OR when the cards data array is reset in the parent
   * @param fromRelease
   */
  const reset = (fromRelease: boolean) => {
    "worklet";
    likeOpacity.value = 0;
    nopeOpacity.value = 0;

    if (fromRelease) {
      translateX.value = withSpring(0);
      rotation.value = withSpring(0);
    } else {
      translateX.value = 0;
      rotation.value = 0;
    }
  };

  /**
   * When a swipe is completed, we need to complete its animation
   * off the screen.
   * We also update the history context
   * @param direction
   */
  const swipeCompleted = (direction: "right" | "left") => {
    "worklet";

    // Figure out the distance we need to push the card off screen
    const toRadians = (angle: number) => angle * (Math.PI / 180);
    const rotatedWidth =
      width * Math.sin(toRadians(90 - ROTATION_DEGREE_RANGE_SIZE)) +
      height * Math.sin(toRadians(ROTATION_DEGREE_RANGE_SIZE));
    const finalX = direction === "right" ? rotatedWidth : -rotatedWidth;

    const finalRotation =
      direction === "right"
        ? -ROTATION_DEGREE_RANGE_SIZE
        : ROTATION_DEGREE_RANGE_SIZE;

    const finalLikeOpacity = direction === "right" ? 1 : 0;
    const finalNopeOpacity = direction === "right" ? 0 : 1;

    likeOpacity.value = finalLikeOpacity;
    nopeOpacity.value = finalNopeOpacity;
    translateX.value = withSpring(finalX);
    rotation.value = withSpring(finalRotation, {}, () => {});

    let arr = history || [];
    arr.push({ ...profile, direction });
    runOnJS(setHistory)(arr);
  };

  /*******************************************
   * Gesture Handling
   *******************************************/

  const gestureHandler = useAnimatedGestureHandler({
    onActive: (e) => {
      // Adjust card x position
      translateX.value = e.translationX;

      // Adjust card rotation
      const rotationValue = interpolate(
        translateX.value,
        [-ROTATION_X_RANGE_SIZE, ROTATION_X_RANGE_SIZE],
        [ROTATION_DEGREE_RANGE_SIZE, -ROTATION_DEGREE_RANGE_SIZE],
        Extrapolate.CLAMP
      );
      rotation.value = rotationValue;

      // Adjust likeOpacity
      const likeValue = interpolate(
        translateX.value,
        [0, ROTATION_X_RANGE_SIZE],
        [0, 1],
        Extrapolate.CLAMP
      );
      likeOpacity.value = likeValue;

      // Adjust nopeOpacity
      const nopeValue = interpolate(
        translateX.value,
        [-ROTATION_X_RANGE_SIZE, 0],
        [1, 0],
        Extrapolate.CLAMP
      );
      nopeOpacity.value = nopeValue;
    },
    onEnd: (e) => {
      if (e.translationX >= ACTION_THRESHOLD && e.velocityX > 0) {
        swipeCompleted("right");
      } else if (e.translationX <= -ACTION_THRESHOLD && e.velocityX < 0) {
        swipeCompleted("left");
      } else {
        reset(true);
      }
    },
  });

  /***************************
   * Animated Styles
   ***************************/

  const cardAnimations = useAnimatedStyle(() => {
    return {
      transform: [
        { translateX: translateX.value },
        { rotateZ: `${rotation.value}deg` },
      ],
    };
  });

  const likeAnimations = useAnimatedStyle(() => {
    return {
      opacity: likeOpacity.value,
    };
  });

  const nopeAnimations = useAnimatedStyle(() => {
    return {
      opacity: nopeOpacity.value,
    };
  });

  /***************************
   * JSX
   ***************************/

  return (
    <Animated.View style={[styles.container, { zIndex }, cardAnimations]}>
      <PanGestureHandler onGestureEvent={gestureHandler}>
        <Animated.View style={[styles.cardInner]}>
          <Card
            profile={profile}
            likeAnimations={likeAnimations}
            nopeAnimations={nopeAnimations}
          />
        </Animated.View>
      </PanGestureHandler>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  cardInner: {
    flex: 1,
  },
  container: {
    flex: 1,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 15,
  },
});

export default CardWrapper;
