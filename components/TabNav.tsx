import React, { useContext } from "react";
import { View, StyleSheet, TouchableOpacity, Text, Alert } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import { HistoryContext } from "./HistoryContext";

import { colors } from "../config/colors";

type TabNavParams = {
  selectedTab: "deck" | "history";
  onChangeTab: (tab: "deck" | "history") => void;
};
function TabNav({ onChangeTab, selectedTab }: TabNavParams) {
  const { history } = useContext(HistoryContext);

  return (
    <View style={styles.topRow}>
      <TouchableOpacity
        disabled={selectedTab === "deck"}
        style={[styles.button, { marginRight: 25 }]}
        onPress={() => {
          onChangeTab("deck");
        }}
      >
        <MaterialCommunityIcons
          name="fire"
          color={
            selectedTab === "deck" ? colors.accent : colors.accentSecondary
          }
          size={32}
        />
      </TouchableOpacity>

      <TouchableOpacity
        disabled={selectedTab === "history"}
        style={[styles.button, { marginLeft: 25 }]}
        onPress={() => {
          Alert.alert(
            "History",
            "History would be opened in a new tab here.",
            [{ text: "OK", onPress: () => console.log("OK Pressed") }],
            { cancelable: false }
          );
        }}
      >
        <MaterialCommunityIcons
          name="history"
          color={
            selectedTab === "history" ? colors.accent : colors.accentSecondary
          }
          size={32}
        />
        {history.length ? (
          <View style={styles.badge}>
            <Text style={styles.badgeText}>{history.length}</Text>
          </View>
        ) : (
          []
        )}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  badge: {
    position: "absolute",
    top: 5,
    right: -2,
    backgroundColor: colors.red,
    paddingHorizontal: 5,
    paddingVertical: 1,
    borderRadius: 7,
  },
  badgeText: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
  },
  button: {
    padding: 10,
  },
  topRow: {
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "center",
  },
});

export default TabNav;
