import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";

import { colors } from "../config/colors";

type EmptyCardParams = {
  onReset: () => void;
};
function EmptyCard({ onReset }: EmptyCardParams) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        You've looked through all the stunt actors we have!
      </Text>
      <Text style={[styles.text, { marginTop: 15 }]}>
        You can start over if you'd like
      </Text>
      <TouchableOpacity style={styles.button} onPress={onReset}>
        <Text style={[styles.text, styles.buttonText]}>Reset</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    backgroundColor: colors.accent,
    marginTop: 25,
  },
  buttonText: {
    color: "white",
  },
  container: {
    flex: 1,
    backgroundColor: "#f3f4f5",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 35,
    marginHorizontal: 15,
  },
  text: {
    fontSize: 18,
    textAlign: "center",
  },
});

export default EmptyCard;
