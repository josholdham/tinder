import React from "react";

import { HistoryItem } from "../types";

export const HistoryContext = React.createContext({
  history: [] as HistoryItem[],
  setHistory: (arr: HistoryItem[]) => {},
});
