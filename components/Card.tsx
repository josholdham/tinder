import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import Animated from "react-native-reanimated";

import { LinearGradient } from "expo-linear-gradient";

import { Profile } from "../types";
import { colors } from "../config/colors";

type CardProps = {
  profile: Profile;
  likeAnimations?: any;
  nopeAnimations?: any;
};
function Card({
  profile,
  likeAnimations = {},
  nopeAnimations = {},
}: CardProps) {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={profile.images[0]} />

      <LinearGradient
        colors={["transparent", "rgba(0,0,0,0.5)"]}
        locations={[0, 0.5]}
        style={styles.overlay}
      >
        <Text style={styles.nameText}>{profile.name}</Text>
      </LinearGradient>

      <Animated.View
        style={[
          styles.actionIndicator,
          styles.actionIndicatorLike,
          likeAnimations,
        ]}
      >
        <Text
          style={[styles.actionIndicatorText, styles.actionIndicatorTextLike]}
        >
          Like
        </Text>
      </Animated.View>
      <Animated.View
        style={[
          styles.actionIndicator,
          styles.actionIndicatorNope,
          nopeAnimations,
        ]}
      >
        <Text
          style={[styles.actionIndicatorText, styles.actionIndicatorTextNope]}
        >
          Nope
        </Text>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  actionIndicator: {
    borderWidth: 4,
    position: "absolute",
    top: 25,
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderRadius: 6,
    opacity: 0,
  },
  actionIndicatorLike: {
    borderColor: colors.accent,
    left: 15,
  },
  actionIndicatorNope: {
    borderColor: colors.red,
    right: 15,
  },
  actionIndicatorText: {
    fontSize: 44,
    textTransform: "uppercase",
    fontWeight: "bold",
    letterSpacing: 5,
  },
  actionIndicatorTextLike: {
    color: colors.accent,
  },
  actionIndicatorTextNope: {
    color: colors.red,
  },
  container: {
    flex: 1,
    backgroundColor: "#f3f4f5",
    borderRadius: 10,
    overflow: "hidden",
    position: "relative",
    shadowColor: "#6472bb",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 3,
    shadowOpacity: 0.1,
  },
  image: {
    flex: 1,
    flexGrow: 1,
    width: "100%",
  },
  nameText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 28,
  },
  overlay: {
    position: "absolute",
    bottom: 0,
    padding: 15,
    paddingVertical: 20,
    left: 0,
    right: 0,
  },
});

export default Card;
