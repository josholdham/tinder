export type Profile = {
  id: string;
  name: string;
  images: number[];
};

export interface HistoryItem extends Profile {
  direction: "left" | "right";
}
