import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  ActivityIndicator,
} from "react-native";

import Constants from "expo-constants";
import { Asset } from "expo-asset";

import Deck from "../components/Deck";
import TabNav from "../components/TabNav";
import { HistoryContext } from "../components/HistoryContext";

import { colors } from "../config/colors";
import { HistoryItem, Profile } from "../types";

const PROFILES: Profile[] = [
  {
    id: "001",
    name: "Jalin Bali",
    images: [require("./../assets/profiles/1.jpeg")],
  },
  {
    id: "002",
    name: "Ryan Matthew",
    images: [require("./../assets/profiles/2.jpeg")],
  },
  {
    id: "003",
    name: "Tenni Zorkhov",
    images: [require("./../assets/profiles/3.jpeg")],
  },
  {
    id: "004",
    name: "Viktoria Kudinska",
    images: [require("./../assets/profiles/4.jpeg")],
  },
  {
    id: "005",
    name: "Fabian Centino",
    images: [require("./../assets/profiles/5.jpeg")],
  },
  {
    id: "006",
    name: "Georgie Smalls",
    images: [require("./../assets/profiles/6.jpeg")],
  },
  {
    id: "007",
    name: "Apsana Sakar",
    images: [require("./../assets/profiles/7.jpeg")],
  },
  {
    id: "008",
    name: "Tarin Vaynor",
    images: [require("./../assets/profiles/8.jpeg")],
  },
  {
    id: "009",
    name: "Cameron Foster",
    images: [require("./../assets/profiles/9.jpeg")],
  },
  {
    id: "010",
    name: "Mike Shin",
    images: [require("./../assets/profiles/10.jpeg")],
  },
];

function Home() {
  const [history, setHistory] = useState<HistoryItem[]>([]);
  const [selectedTab, setSelectedTab] = useState<"deck" | "history">("deck");
  const [ready, setReady] = useState<boolean>(false);

  /***************************
   * Image pre-Loading
   ***************************/

  const loadImages = async (images: number[]) => {
    await Promise.all(images.map((img) => Asset.loadAsync(img)));
    setReady(true);
  };

  useEffect(() => {
    // Pre-Load Images on init
    const imageArrs = PROFILES.map((profile) => profile.images);
    const images = imageArrs.flat();
    loadImages(images);
  }, []);

  /***************************
   * JSX
   ***************************/

  const renderDeck = () => {
    return <Deck data={PROFILES} />;
  };

  const renderLoading = () => {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator
          color={colors.accentSecondary}
          size="large"
        ></ActivityIndicator>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <HistoryContext.Provider value={{ history, setHistory }}>
        <TabNav selectedTab={selectedTab} onChangeTab={setSelectedTab} />
        {ready ? renderDeck() : renderLoading()}
        <View style={styles.bottomRow}>
          {/**  TODO: add like buttons etc */}
        </View>
      </HistoryContext.Provider>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  bottomRow: {
    height: 15,
  },

  container: {
    flex: 1,
    paddingVertical: 30,
    paddingTop: Constants.statusBarHeight,
  },

  loadingContainer: {
    flex: 1,
    justifyContent: "center",
  },
});

export default Home;
