# Tinder Swipe

A demo project to implement a basic Tinder UI. It is a React Native project built with expo.

## Demo / Running

Pull the repo, `yarn install` and then `yarn start`.

Alternatively a built version can be accessed on Expo [here](https://expo.io/@josholdham/projects/PlanesTinder?release-channel=staging).

**Note**: This project has only been tested on iOS.

## Motivation

There are plenty of demo solutions out there using the standard React native `Animated` and `PanResponder` APIs.

Although I feel more confident using those APIs, I decided to opt for a solution using `react-native-reanimated v2`, and `react-native-gesture-handler` for the following reasons:

1. These packages allow animations to run on the UI thread, preventing blocking from activity on the JS thread, and allowing for closer to 60FPS animations - which I think is crucial in an animation-lead project such as this.
2. I hadn't yet used it - so I wanted to learn it :-)
3. I haven't seen any solutions/tutorials using these libraries online, so hopefully what I've created is relatively unique.

There are of course a number of extra features I would have liked to have added, and code hygeine could perhaps be better (all this can be discussed), but there we go.

## Improvements

- I'd quite like to have had the Deck cmponent handle the gesture and animation logic. It feels tidier, and also would make it easier for different cards to respond to each others' gesture events more easily (ie if we want to scale the second card as the top card drags off screen). I had attempted this but my solution wasn't quite right: I don't think different elements can share the same useAnimatedStyle(), and re-rendering the stack array (so that only one element at a time had it assigned to it) caused image glitching which I'm sure could be removed, but I decided to abandon for time.
- It'd be good to abstract more styles to the config folder, such as font size, padding sizes, and so on.
- I ended up in a bit of a messy halfway house between component flexibility/ re-usability and not: for instance the deck took in a data prop but probably could have taken in a Card component as well (or a render Card function). Things like that.
